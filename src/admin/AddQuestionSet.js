import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Submit, Text } from '@danderson00/react-forms-material'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'
import { v4 as uuid } from 'uuid'

export default publisher(
  function AddQuestion({ open, onClose, publish }) {
    const add = ({ name }) => {
      const questionSetId = uuid()

      return publish('questionSet', { questionSetId, name })
        .then(() => publish('questionSetCode', { questionSetId }))
        .then(() => publish('questionSetSelected', { questionSetId }))
        .then(onClose)
    }

    return (
      <Dialog open={open} onClose={onClose}>
        <Form onSubmit={add} resetOnSubmit>
          <DialogTitle>Add Question Set</DialogTitle>
          <DialogContent>
            <Text name="name" label="Name" required autoFocus />
          </DialogContent>

          <DialogActions>
            <Submit>Add</Submit>
            <Button onClick={onClose}>Cancel</Button>
          </DialogActions>
        </Form>
      </Dialog>
    )
  }
)
