import React from 'react'
import { Box } from '@material-ui/core'
// import logo from '../assets/logo.png'
import AccountMenu from '../account/AccountMenu'
import DisplayOpener from './DisplayOpener'

export default ({ header, leftPanel, children }) => (
  <div className="admin">
    <Box display="flex" flexDirection="row" alignItems="center" justify="space-between" className="mb1 p1">
      <Box flexGrow={1}>
        <h1>hordesauce</h1>
        {/*<img src={logo} alt="Logo" className="logo" />*/}
      </Box>
      {header}
      <DisplayOpener />
      <AccountMenu />
    </Box>

    <Box display="flex" flexDirection="row">
      {leftPanel &&
        <Box className="left-panel mr2">
          {leftPanel}
        </Box>
      }
      <Box flexGrow={1}>
        {children}
      </Box>
    </Box>
  </div>
)
