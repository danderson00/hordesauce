import React from 'react'
import { connect } from '@x/unify.react'
import { List, ListItem } from '@material-ui/core'
import Response from './Response'

export default connect(({ type }) => type)(
  function Responses(props) {
    return (
      <List>
        {props[props.type].responses.map(
          response => (
            <ListItem key={response.responseId} className="row">
              <Response {...response} />
            </ListItem>
          )
        )}
      </List>
    )
  }
)
