import React from 'react'
import { connect } from '@x/unify.react'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'
import { Form, Submit, Text } from '@danderson00/react-forms-material'

export default connect(({ query }) => query)(
  function Export({ query, open, onClose, ...props }) {
    const exportQuestion = props[query]

    const performExport = ({ filename }) => {
      exportCsv(
        query === 'exportComments'
          ? flattenMultiple(exportQuestion, 'comments')
          : flattenMultiple(exportQuestion.responses, 'scores'),
        filename
      )
      onClose()
    }

    const defaultFilename = exportQuestion.question
      ? `${exportQuestion.question.replace(/[^a-z0-9]/gi, '_')}-${getTimestamp()}.csv`
      : `comments-${getTimestamp()}.csv`

    const dataExists = (exportQuestion.responses && exportQuestion.responses.length > 0) || exportQuestion.length > 0

    return (
      <Dialog open={open} onClose={onClose}>
        <Form onSubmit={performExport}>
          <DialogTitle>Export to CSV</DialogTitle>
          <DialogContent>
            {dataExists
              ? <Text name="filename" label="Filename" defaultValue={defaultFilename} required />
              : 'There is no data to export'
            }
          </DialogContent>
          <DialogActions>
            {dataExists > 0 && <Submit>Export</Submit>}
            <Button onClick={onClose}>Cancel</Button>
          </DialogActions>
        </Form>
      </Dialog>
    )
  }
)

const flattenMultiple = (array, childProperty) => array.reduce(
  (flattened, target) => [
    ...flattened,
    ...flatten(target, childProperty)
  ],
  []
)

const flatten = (target, childProperty) => target[childProperty] && target[childProperty].length > 0
  ? target[childProperty].map(x => ({ ...except(target, childProperty), ...x }))
  : [except(target, childProperty)]

const except = (target, property) => (({ [property]: _, ...result }) => result)(target)

function getTimestamp() {
  const date = new Date()
  const year = date.getFullYear()
  const month = `${date.getMonth() + 1}`.padStart(2, '0')
  const day =`${date.getDate()}`.padStart(2, '0')
  const hour = `${date.getHours()}`.padStart(2, '0')
  const minutes = `${date.getMinutes()}`.padStart(2, '0')
  return `${year}${month}${day}-${hour}${minutes}`
}

const exportCsv = (data, filename) => {
  const headers = Object.keys(data[0]).map(x => `"${x}"`).join(',')
  const content = data.map(
    x => Object.values(x)
      .map(value => typeof value === 'string' ? `"${value.replace(/"/g, "\\\"")}"` : value)
      .join(',')
  ).join('\n')
  const csv = `${headers}\n${content}`

  const hiddenElement = document.createElement('a')
  hiddenElement.href = 'data:text/csvcharset=utf-8,' + encodeURI(csv)
  hiddenElement.target = '_blank'
  hiddenElement.download = filename
  hiddenElement.click()
}
