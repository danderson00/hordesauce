import React from 'react'
import { publisher, connect } from '@x/unify.react'
import { Card, Box, Paper, CardContent, Chip } from '@material-ui/core'
import AddToQueue from '@material-ui/icons/AddToQueue'
import DialogButton from '../common/DialogButton'
import AddQuestionSet from './AddQuestionSet'
import Syncing from '../common/Syncing'

export default publisher(connect('questionSets')(
  function QuestionSets({ questionSets, publish, synced }) {
    const selectQuestionSet = questionSetId => publish('questionSetSelected', { questionSetId })

    return <Paper className="p2">
      {!synced ? <Syncing /> : questionSets.map(({ questionSetId, name, code, questionCount }) =>
        <Card
          key={questionSetId}
          onClick={() => selectQuestionSet(questionSetId)}
          className="clickable mb2"
        >
          <CardContent>
            <Box display="flex" flexDirection="row">
              <Box flexGrow={1}>{name}</Box>
              <Box>
                {questionCount} question{questionCount === 1 ? '' : 's'}&nbsp;
                (code: <Chip className="code" label={code} size="small" />)
              </Box>
            </Box>
          </CardContent>
        </Card>
      )}
      <DialogButton component={AddQuestionSet} startIcon={<AddToQueue />}>
        Add new question set
      </DialogButton>
    </Paper>
  }
))
