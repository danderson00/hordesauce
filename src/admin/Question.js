import React from 'react'
import { publisher, connect } from '@x/unify.react'
import { Card, CardContent, Checkbox } from '@material-ui/core'
import PlaylistAdd from '@material-ui/icons/PlaylistAdd'
import { Form, Text } from '@danderson00/react-forms-material'
import QuestionActions from './QuestionActions'
import AddResponse from '../common/AddResponse'
import DialogButton from '../common/DialogButton'
import Responses from './Responses'
import questionTypes from '../questionTypes'
import Syncing from '../common/Syncing'

export default publisher(connect('question')(
  function Question({ question, active, publish, synced }) {
    const { type, explanatoryText, text, responseCount, allowClientAdd, allowComments } = question
    const questionType = questionTypes[type] || {}

    const updateQuestionText = ({ text }) => publish('question', { text })
    const updateExplanatoryText = ({ explanatoryText }) => publish('question', { explanatoryText })
    const toggleComments = () => publish('question', { allowComments: !allowComments })
    const toggleAllowAdd = () => publish('question', { allowClientAdd: !allowClientAdd })
      .then(() => {
        if(active && responseCount === 0) {
          publish('questionActivated', { questionId: undefined })
        }
      })

    return !synced ? <Syncing /> : <Card><CardContent>
      <div className="row space-between question-header">
        <Form onSubmit={updateQuestionText} className="flex">
          <Text name="text" value={text} required fullWidth submitOnBlur />
        </Form>
        <QuestionActions question={question} active={active} />
      </div>

      <div className="row">
        <small className="flex">{questionType.displayText}</small>
        <label>
          <Checkbox checked={allowClientAdd || false} onChange={toggleAllowAdd} />
          Allow participants to add new responses
        </label>
        <label>
          <Checkbox checked={allowComments || false} onChange={toggleComments} />
          Allow comments
        </label>
      </div>

      <Form onSubmit={updateExplanatoryText} className="flex">
        <Text name="explanatoryText" label="Explanatory text" value={explanatoryText} fullWidth submitOnBlur />
      </Form>

      {responseCount > 0
        ? <Responses type={type} />
        : <p>No responses have been added</p>
      }

      <div className="row">
        <DialogButton component={AddResponse} startIcon={<PlaylistAdd />}>
          Add Response
        </DialogButton>
      </div>
    </CardContent></Card>
  }
))
