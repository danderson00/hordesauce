import React from 'react'
import { Navigator } from '@x/unify.react'
import navigation from './navigation'

export default function Admin() {
  return <Navigator
    machine={navigation}
    resolveComponent={path => import(/* webpackPrefetch: true */ `./${path}`).then(m => m.default)}
  />
}