import { Machine } from '@x/unify.react'

export default Machine({
  initial: 'Home',
  on: {
    'home': 'Home'
  },
  states: {
    'Home': {
      on: { 'questionSetSelected': 'QuestionSet' }
    },
    'QuestionSet': {
      meta: {
        scope: ['questionSetId']
      }
    }
  }
})