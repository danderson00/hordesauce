import React from 'react'
import { publisher } from '@x/unify.react'
import { Button } from '@material-ui/core'

export default publisher(
  function QuestionActions({ question, active, publish }) {
    const { questionId, responseCount, showScores, locked, showNew, allowClientAdd } = question

    const toggleActivated = () => publish('questionActivated', { questionId: active ? undefined : questionId })
    const toggleHidden = () => publish('question', { showScores: !showScores })
    const toggleLocked = () => publish('question', { locked: !locked })
    const toggleNew = () => publish('question', { showNew: !showNew })

    return <>
      <ToggleButton
        onClick={toggleActivated}
        active={active}
        activeText="Deactivate"
        activeColor="secondary"
        inactiveText="Activate"
        disabled={responseCount === 0 && !allowClientAdd}
      />

      <ToggleButton
        onClick={toggleLocked}
        active={locked}
        activeText="Unlock"
        activeColor="secondary"
        inactiveText="Lock"
      />

      <ToggleButton
        onClick={toggleHidden}
        active={showScores}
        activeText="Hide scores"
        inactiveText="Show scores"
      />

      <ToggleButton
        onClick={toggleNew}
        active={showNew}
        activeText="Hide new"
        inactiveText="Show new"
      />
    </>
  }
)

const ToggleButton = ({ onClick, active, text, activeText, activeColor = 'primary', inactiveText, disabled }) => (
  <Button
    onClick={onClick}
    color={active ? activeColor : 'default'}
    variant="contained"
    disabled={disabled}
  >
    {text || (active ? activeText : inactiveText)}
  </Button>
)