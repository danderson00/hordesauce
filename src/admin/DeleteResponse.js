import React from 'react'
import { publisher } from '@x/unify.react'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'

export default publisher(
  function DeleteResponse({ responseId, text, open, onClose, publish }) {
    const deleteResponse = () => publish('response', { responseId, deleted: true }).then(onClose)

    return (
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>Delete Response?</DialogTitle>
        <DialogContent>Are you sure you wish to delete the response <b>{text}</b>?</DialogContent>
        <DialogActions>
          <Button onClick={deleteResponse}>Delete</Button>
          <Button onClick={onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    )
  }
)
