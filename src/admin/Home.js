import React from 'react'
import Shell from './Shell'
import QuestionSets from './QuestionSets'

export default function QuestionSet() {
  return (
    <Shell>
      <QuestionSets />
    </Shell>
  )
}
