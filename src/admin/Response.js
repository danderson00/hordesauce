import React from 'react'
import { publisher } from '@x/unify.react'
import Score from '../common/Score'
import { Form, Text } from '@danderson00/react-forms-material'
import DeleteResponse from './DeleteResponse'
import DialogButton from '../common/DialogButton'
import DeleteIcon from '../common/DeleteIcon'

export default publisher(
  function Response({ responseId, text, score, min, max, publish }) {
    const updateText = ({ text }) => publish('response', { responseId, text })

    return <>
      <Form onSubmit={updateText} className="flex">
        <Text name="text" value={text} fullWidth required submitOnBlur />
      </Form>

      <Score value={score} min={min} max={max} />

      <DialogButton
        component={DeleteResponse}
        variant="text"
        responseId={responseId}
        text={text}
      >
        <DeleteIcon />
      </DialogButton>

    </>
  }
)
