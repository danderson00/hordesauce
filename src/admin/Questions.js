import React from 'react'
import { publisher, connect } from '@x/unify.react'
import { Button, Card, CardContent, Box, Paper, Chip } from '@material-ui/core'
import ArrowBack from '@material-ui/icons/ArrowBack'
import PostAdd from '@material-ui/icons/PostAdd'
import DialogButton from '../common/DialogButton'
import AddQuestion from './AddQuestion'
import Syncing from '../common/Syncing'
import alertOn from '../assets/alert-on.png'
import alertOff from '../assets/alert-off.png'

export default publisher(connect('questionSet', 'questions', 'selectedQuestionId', 'activeQuestionId')(
  function Questions({ questionSet, questions, selectedQuestionId, activeQuestionId, publish, synced }) {
    const selectQuestion = questionId => publish('questionSelected', { questionId })

    return !synced ? <Syncing /> : (
      <Paper className="column p2">
        <Button
          fullWidth
          onClick={() => publish('home')}
          variant="contained"
          startIcon={<ArrowBack />}
        >
          <Box flexGrow={1}>Back</Box>
        </Button>

        <Box display="flex" flexDirection="row" className="pt1">
          <Box flexGrow={1}><h3>{questionSet.name}</h3></Box>
          <Box><Chip className="code" label={questionSet.code} size="small" /></Box>
        </Box>

        {questions.map(({ questionId, text }) =>
          <Card
            key={questionId}
            onClick={() => selectQuestion(questionId)}
            className="clickable"
          >
            <CardContent>
              <Box display="flex" flexDirection="row" alignItems="center">
                <Box flexGrow={1}>
                  {questionId === selectedQuestionId
                    ? <b>{text}</b>
                    : text
                  }
                </Box>
                <img
                  src={questionId === activeQuestionId ? alertOn : alertOff}
                  style={{ width: 20, height: 20 }}
                  alt="active status"
                />
              </Box>
            </CardContent>
          </Card>
        )}

        <DialogButton component={AddQuestion} startIcon={<PostAdd />}>
          <Box flexGrow={1}>Add Question</Box>
        </DialogButton>
      </Paper>
    )
  }
))