import React from 'react'
import Shell from './Shell'
import { publisher } from '@x/unify.react'
import Questions from './Questions'
import SelectedQuestion from './SelectedQuestion'

export default publisher(function QuestionSet({ publish }) {
  return (
    <Shell
      header={<>
        {/*<IconButton onClick={() => publish('questionActivated')} className="mr2">*/}
        {/*  Deactivate*/}
        {/*</IconButton>*/}
      </>}
      leftPanel={<Questions />}
    >
      <SelectedQuestion />
    </Shell>
  )
})
