import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Submit, Text, Radio } from '@danderson00/react-forms-material'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'
import { v4 as uuid } from 'uuid'
import types from '../questionTypes'

export default publisher(
  function AddQuestion({ open, onClose, publish }) {
    const add = ({ text, type }) => {
      const questionId = uuid()

      return publish('question', { questionId, text, type, ...types[type].defaultOptions })
        .then(() => publish('questionSelected', { questionId }))
        .then(onClose)
    }

    return (
      <Dialog open={open} onClose={onClose}>
        <Form onSubmit={add} resetOnSubmit>
          <DialogTitle>Add Question</DialogTitle>
          <DialogContent>
            <Text name="text" label="Question" required multiline rows={3} autoFocus />
            <Radio
              name="type"
              label="Type"
              defaultValue="crowdsource"
              values={Object.keys(types)}
              labels={Object.keys(types).map(x => types[x].displayText)}
              required
            />
          </DialogContent>

          <DialogActions>
            <Submit>Add</Submit>
            <Button onClick={onClose}>Cancel</Button>
          </DialogActions>
        </Form>
      </Dialog>
    )
  }
)
