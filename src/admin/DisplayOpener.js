import React from 'react'
import { connect } from '@x/unify.react'
import { IconButton } from '@material-ui/core'
import OpenInNew from '@material-ui/icons/OpenInNew'

export default connect('selectedQuestionSetId')(
  ({ selectedQuestionSetId }) => (
    <IconButton
      onClick={() => window.open('/display/' + selectedQuestionSetId)}
      disabled={!selectedQuestionSetId}
    >
      <OpenInNew />
    </IconButton>
  )
)