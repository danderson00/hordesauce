import React from 'react'
import { connect } from '@x/unify.react'
import Question from './Question'
import { Card, CardContent } from '@material-ui/core'

export default connect('selectedQuestionId', 'activeQuestionId')(
  function SelectedQuestion({ selectedQuestionId, activeQuestionId }) {
    return selectedQuestionId
      ? <Question
          scope={{ questionId: selectedQuestionId }}
          active={selectedQuestionId === activeQuestionId}
          key={selectedQuestionId}
        />
      : <Card><CardContent>
          Select a question from the left or add a new one.
        </CardContent></Card>
  }
)