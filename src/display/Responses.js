import React from 'react'
import { connect } from '@x/unify.react'
import { Button, Card, CardContent } from '@material-ui/core'
import Score from '../common/Score'

export default connect(({ type }) => type)(
  function Responses(props) {
    const { responses, showNew } = props[props.type]
    const byScore = (a, b) => b.score - a.score
    const byAge = (a, b) => b.respondedAt - a.respondedAt

    const renderResponses = items => (
      items.map(
        ({ responseId, text, score, max, min }) => (
          <Card key={responseId}>
            <CardContent className="row">
              <h4 className="flex">{text}</h4>
              <Score
                value={score}
                max={max}
                min={min}
              />
            </CardContent>
          </Card>
        )
      )
    )

    return (
      <div className="row">
        <div className="flex">
          {showNew && <Card className="button-card"><Button variant="contained" color="primary" fullWidth><b>TOP</b></Button></Card>}
          {renderResponses(responses.sort(byScore))}
        </div>
        {showNew &&
          <div className="flex">
            <Card className="button-card"><Button variant="contained" color="primary" fullWidth><b>NEW</b></Button></Card>
            {renderResponses(responses.sort(byAge))}
          </div>
        }
      </div>
    )
  }
)
