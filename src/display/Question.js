import React from 'react'
import { connect } from '@x/unify.react'
import Responses from './Responses'
import { Card, CardContent } from '@material-ui/core'

export default connect('question')(
  ({ question }) => <>
    <Card><CardContent>
      <h2>{question.text}</h2>
    </CardContent></Card>

    {question.responseCount > 0 && question.showScores
      ? <Responses type={question.type} />
      : <Card><CardContent>
          Add your responses now by opening <a href="https://hordesauce.com/">https://hordesauce.com/</a> on your phone.
        </CardContent></Card>
    }
  </>
)
