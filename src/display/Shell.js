import React from 'react'
import { connect } from '@x/unify.react'
import Question from './Question'
import { Card, CardContent } from '@material-ui/core'
// import logo from '../assets/logo.png'

export default connect('activeQuestionId')(
  ({ activeQuestionId }) =>
    <div className="display">
      {/*<img src={logo} alt="Logo" className="logo" />*/}
      <h1>Hordesauce</h1>

      {activeQuestionId
        ? <Question scope={{ questionId: activeQuestionId }} key={activeQuestionId} />
        : <Card><CardContent>
            <h3>Welcome to hordesauce!</h3>
            <p>
              You can get ready to answer questions by opening <a
              href="https://hordesauce.com/">https://hordesauce.com/</a> on your phone.
            </p>
          </CardContent></Card>}
    </div>
)