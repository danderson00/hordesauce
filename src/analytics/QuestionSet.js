import React from 'react'
import { connect } from '@x/unify.react'
import Question from "./Question";

export default connect('questionSet', 'questions')(
  (questionSet, questions) => (
    <div>
      {questions.map(({ questionId }) =>
        <Question scope={{ questionId }} />
      )}
    </div>
  )
)
