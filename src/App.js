import React from 'react'
import { auth } from '@x/unify.react'
import Admin from './admin'
import Client from './client/QuestionCodeWrapper'
import Display from './display/Shell'
import Marketing from './marketing'
import './styles'

export default auth(({ auth: { authenticated } }) => component(authenticated))

const component = (authenticated) => {
  const path = window.location.pathname.toLowerCase()
  const firstPathSegment = path.substring(1, path.indexOf('/', 1))
  const lastPathSegment = path.substring(path.lastIndexOf('/') + 1)

  switch(true) {
    case firstPathSegment === 'display': return <Display scope={{ questionSetId: lastPathSegment }} />
    case firstPathSegment === 'go': return <Client scope={{ questionSetCode: lastPathSegment.toUpperCase() }} />
    case authenticated: return <Admin />
    default: return <Marketing />
  }
}
