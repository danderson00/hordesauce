import React from 'react'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'
import { Form, Submit, Text } from '@danderson00/react-forms-material'

export default function ResponderDetails({ open, onClose }) {
  if(open) {
    localStorage.clientNameRequested = true
  }

  const update = ({ name }) => {
    localStorage.clientName = name
    onClose()
  }

  return (
    <Dialog open={open} onClose={onClose}>
      <Form onSubmit={update} resetOnSubmit>
        <DialogTitle>Your Details</DialogTitle>
        <DialogContent>
          <p>
            Enter your name below. This is entirely optional, and all responses
            remain anonymous if you do not provide it.
          </p>
          <Text name="name" label="Name" value={localStorage.clientName} autoFocus fullWidth />
        </DialogContent>
        <DialogActions>
          <Submit>Save</Submit>
          <Button onClick={onClose}>Close</Button>
        </DialogActions>
      </Form>
    </Dialog>
  )
}