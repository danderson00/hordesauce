import React from 'react'
import { connect } from '@x/unify.react'
import { Card, CardContent } from '@material-ui/core'
import Crowdsource from './types/Crowdsource'
import MultipleChoice from './types/MultipleChoice'
import Ratings from './types/Ratings'
import DialogButton from '../common/DialogButton'
import AddResponse from '../common/AddResponse'
import AddComment from './AddComment'
import Syncing from '../common/Syncing'

export default connect('question')(
  function Question({ question, synced }) {
    const { text, explanatoryText, type, responseCount, locked, allowClientAdd, allowComments } = question

    const componentFor = type => {
      switch(type) {
        case 'crowdsource': return <Crowdsource />
        case 'multipleChoice': return <MultipleChoice />
        case 'ratings': return <Ratings />
        default: throw new Error(`Unknown type ${type}`)
      }
    }

    return !synced ? <Syncing /> : <>
      <Card>
        <CardContent>
          <h2>{text}</h2>
          {explanatoryText && <p>{explanatoryText}</p>}
        </CardContent>
      </Card>

      {(allowClientAdd && !locked)
        ? <Card>
            <DialogButton component={AddResponse} fullWidth>
              Add Response
            </DialogButton>
          </Card>
        : undefined
      }

      {responseCount > 0
        ? componentFor(type)
        : <Card><CardContent>
            <h3>No responses have been added yet.</h3>
            {allowClientAdd && <p>You can add one by clicking the "Add Response" button above.</p>}
          </CardContent></Card>
      }

      {(allowComments && !locked) && <AddComment scope={{ clientId: window.clientId }} />}
    </>
  }
)
