import React from 'react'
import { publisher, connect } from '@x/unify.react'
import { Card, Button, ButtonGroup, CardContent } from '@material-ui/core'
import Responses from '../Responses'
import Score from '../../common/Score'

export default connect('crowdsource')(
  function Crowdsource({ crowdsource }) {
    return <Responses
      responses={crowdsource.responses}
      showSort={!crowdsource.locked && crowdsource.showScores}
      item={response =>
        <CrowdsourceItem
          response={response}
          scope={{ responseId: response.responseId, clientId: window.clientId }}
          showScores={crowdsource.showScores}
          locked={crowdsource.locked}
        />
      }
    />
  }
)

const CrowdsourceItem = publisher(connect('crowdsourceScore')(
  ({ crowdsourceScore, response, showScores, locked, publish }) => {
    const button = (value, text) => {
      const active = crowdsourceScore === value
      const score = () => publish('score', {
        value: active ? 0 : value,
        clientName: localStorage.clientName
      })

      return <Button
        onClick={score}
        color={active ? 'primary' : 'default'}
        variant="contained"
      >
        {text}
      </Button>
    }

    const { score, text, min, max } = response

    return <Card className="row">
      {locked ? undefined :
        <ButtonGroup orientation="vertical">
          {button(1, '+')}
          {button(-1, '-')}
        </ButtonGroup>
      }
      <CardContent className="flex p1">{text}</CardContent>
      {showScores && <Score value={score} max={max} min={min} width={50} />}
    </Card>
  }
))