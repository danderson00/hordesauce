import React from 'react'
import { publisher, connect, useEvaluate } from '@x/unify.react'
import { Card, CardContent, Button } from '@material-ui/core'
import Score from '../../common/Score'
import Responses from '../Responses'

export default publisher(connect('multipleChoice')(
  function MultipleChoice({ multipleChoice, publish, scope }) {
    const { responses, showScores, locked } = multipleChoice

    const [selectedResponseId] = useEvaluate(
      'multipleChoiceSelectedResponse',
      { ...scope, clientId: window.clientId },
      []
    )

    const selectResponse = responseId => publish('score', {
      responseId,
      clientId: window.clientId,
      clientName: localStorage.clientName
    })

    return <Responses
      responses={responses}
      showSort={!locked && showScores}
      item={({ responseId, text, score, max }) => <Card className="row">
        {locked ? undefined :
          <Button
            onClick={() => selectResponse(responseId)}
            color={selectedResponseId === responseId ? 'primary' : 'default'}
            variant="contained"
          >
            +
          </Button>
        }

        <CardContent className="flex p1">{text}</CardContent>

        {showScores &&
          <Score
            width={50}
            value={score}
            max={max}
          />
        }
      </Card>}
    />
  }
))
