import React from 'react'
import { publisher, connect } from '@x/unify.react'
import { Button, ButtonGroup, Card, CardContent } from '@material-ui/core'
import Responses from '../Responses'

export default connect('ratings')(
  function Ratings({ ratings }) {
    return <Responses
      responses={ratings.responses}
      showSort={false}
      item={response =>
        <RatingsItem
          response={response}
          scope={{ responseId: response.responseId, clientId: window.clientId }}
          locked={ratings.locked}
        />
      }
    />
  }
)

const RatingsItem = publisher(connect('ratingsScore')(
  ({ ratingsScore, response, locked, publish }) => {
    const rate = ({ value }) => publish('score', { value, clientName: localStorage.clientName })

    // Button must be a direct descendant of ButtonGroup - use a props object instead of a custom component
    const buttonProps = value => ({
      children: value,
      color: ratingsScore === value ? 'primary' : 'default',
      variant: 'contained',
      onClick: () => rate({ value })
    })

    return <Card>
      <CardContent>{response.text}</CardContent>
      {locked ? undefined :
        <ButtonGroup fullWidth>
          <Button {...buttonProps(1)} />
          <Button {...buttonProps(2)} />
          <Button {...buttonProps(3)} />
          <Button {...buttonProps(4)} />
          <Button {...buttonProps(5)} />
        </ButtonGroup>
      }
    </Card>

  }
))
