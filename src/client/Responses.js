import React, { useState } from 'react'
import { Card, Button, ButtonGroup } from '@material-ui/core'

export default function Responses({ responses, item, showSort }) {
  const sort = ({ property, descending }) => (a, b) => descending
    ? b[property] - a[property]
    : a[property] - b[property]

  const [sortProperty, setSortProperty] = useState({ property: 'respondedAt', descending: false })
  const sorted = showSort ? responses.sort(sort(sortProperty)) : responses

  const sortButton = (property, descending, text) => (
    <Button
      onClick={() => setSortProperty({ property, descending })}
      color={(sortProperty.property === property && sortProperty.descending === descending) ? 'primary' : 'default'}
    >{text}</Button>
  )

  return <>
    {showSort === false ||
      <Card>
        <ButtonGroup fullWidth variant="contained">
          {sortButton('respondedAt', false, 'First')}
          {sortButton('respondedAt', true, 'New')}
          {sortButton('score', true, 'Top')}
        </ButtonGroup>
      </Card>
    }

    {sorted.map(response => (
      <div key={response.responseId} className="response">
        {item(response)}
      </div>
    ))}
  </>
}

