import React, { useState } from 'react'
import { connect } from '@x/unify.react'
import Question from './Question'
import { Box, Card, CardContent, IconButton } from '@material-ui/core'
import AccountCircle from '@material-ui/icons/AccountCircle'
import ResponderDetails from './ResponderDetails'
// import logo from '../assets/logo.png'

export default connect('activeQuestionId')(
  ({ activeQuestionId }) => {
    const [detailsOpen, setDetailsOpen] = useState(!localStorage.clientNameRequested)

    return <div className="client">
      <div>
        {/*<img src={logo} alt="Logo" className="logo" />*/}
        <Box display="flex" flexDirection="row" className="pb2">
          <Box flexGrow={1}><h1>Hordesauce</h1></Box>
          <IconButton onClick={() => setDetailsOpen(true)} className="p0">
            <AccountCircle fontSize="large" />
          </IconButton>
        </Box>

        {activeQuestionId
          ? <Question scope={{ questionId: activeQuestionId }} key={activeQuestionId} />
          : <Card><CardContent>
              <h3>Welcome to hordesauce!</h3>
              <p>When the presenter is ready, you will be able to respond to polls here.</p>
            </CardContent></Card>
        }
      </div>

      <ResponderDetails open={detailsOpen} onClose={() => setDetailsOpen(false)} />
    </div>
  }
)
