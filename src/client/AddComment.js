import React, { useState } from 'react'
import { publisher } from '@x/unify.react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'
import { Button, Card, CardContent, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'

export default publisher(
  function Ratings({ comment, publish }) {
    const [showConfirmation, setShowConfirmation] = useState(false)

    const submitComment = ({ text }) => publish('comment', { text, clientName: localStorage.clientName })
      .then(() => setShowConfirmation(true))

    return <>
      <Card>
        <CardContent>
          <Form onSubmit={submitComment} resetOnSubmit>
            <Text name="text" value={comment} label="Comments" multiline rows={3} required fullWidth />
            <Submit variant="contained">Submit</Submit>
          </Form>
        </CardContent>
      </Card>

      <Confirmation open={showConfirmation} onClose={() => setShowConfirmation(false)} />
    </>
  }
)

const Confirmation = ({ open, onClose }) => (
  <Dialog open={open} onClose={onClose}>
    <DialogTitle>Comment Submitted</DialogTitle>
    <DialogContent>Your comment was submitted.</DialogContent>
    <DialogActions>
      <Button onClick={onClose}>Close</Button>
    </DialogActions>
  </Dialog>
)