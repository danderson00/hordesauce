import React from 'react'
import { connect } from '@x/unify.react'
import { Card, CardContent } from '@material-ui/core'
import Shell from './Shell'

export default connect('questionSetIdFromCode')(
  function SelectedQuestion({ questionSetIdFromCode }) {
    return questionSetIdFromCode
      ? <Shell resetScope={{ questionSetId: questionSetIdFromCode }} />
      : <Card><CardContent>
          No question set exists for the specified code.
        </CardContent></Card>
  }
)