import React from 'react'
import Login from '../account/Login'
import DialogButton from '../common/DialogButton'
import { Button } from '@material-ui/core'
import './marketing.css'
// import Auth0Login from '../account/Auth0Login'

export default () => (
  <div className="Marketing">
    <header>
      <input id="code" />
      <Button onClick={() => window.location.pathname = `go/${document.querySelector('#code').value}`}>Go</Button>
      {/*<Auth0Login />*/}
      <DialogButton component={Login} variant="text">Login</DialogButton>
    </header>
    <article>
      <h1>hordesauce</h1>
    </article>
  </div>
)