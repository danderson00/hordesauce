module.exports = ({ scopeOwnerOnly, required, userId, generated }) => ({
  question: [scopeOwnerOnly('questionSetId'), {
    userId,
    questionId: [required, String],
    questionSetId: [required, String],
    text: String,
    type: String,
    explanatoryText: String,
    allowComments: Boolean,
    allowClientAdd: Boolean,
    showScores: Boolean,
    locked: Boolean,
    showNew: Boolean
  }],
  questionSelected: [scopeOwnerOnly('questionSetId'), {
    userId,
    questionSetId: [required, String],
    questionId: [required, String]
  }],
  questionActivated: [scopeOwnerOnly('questionSetId'), {
    userId,
    questionSetId: [required, String],
    questionId: String
  }],
  questionSet: [scopeOwnerOnly('questionSetId'), {
    userId,
    questionSetId: [required, String],
    name: String
  }],
  questionSetCode: [scopeOwnerOnly('questionSetId'), {
    userId,
    questionSetId: [required, String],
    questionSetCode: generated(() => Math.random().toString(36).substring(2, 6).toUpperCase().replace(/O/g, '0')) // [A-Z0-9]{4}
  }],
  questionSetSelected: [scopeOwnerOnly('questionSetId'), {
    userId,
    questionSetId: [required, String]
  }],
  
  response: {
    userId,
    questionSetId: [required, String],
    questionId: [required, String],
    responseId: [required, String],
    respondedAt: Number,
    clientId: String,
    clientName: String,
    text: String,
    deleted: Boolean
  },
  score: {
    userId,
    questionSetId: [required, String],
    questionId: [required, String],
    responseId: [required, String],
    clientId: [required, String],
    clientName: String,
    value: Number
  },
  comment: {
    userId,
    questionSetId: [required, String],
    questionId: [required, String],
    clientId: [required, String],
    clientName: String,
    text: String
  }
})