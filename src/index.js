import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { Provider } from '@x/unify.react'
import consumer from '@x/unify'
import { v4 as uuid } from 'uuid'
import { Auth0Provider } from '@auth0/auth0-react'

window.clientId = localStorage['clientId'] = localStorage['clientId'] || uuid()

const url = window.location.hostname.includes('localhost')
  ? 'ws://localhost:3001'
  : `${window.location.protocol === 'http:' ? 'ws' : 'wss'}://${window.location.host}/`

const auth0Config = {
  domain: 'hordesauce.auth0.com',
  clientId: 'c2FTdOZLwkXr97a7f7cajqwyDGwHjkps',
  audience: 'https://hordesauce.com'
}

consumer({ url, log: { unhandled: true } }).connect().then(host =>
  ReactDOM.render(
    <Provider host={host}>
      <Auth0Provider {...auth0Config}>
        <App/>
      </Auth0Provider>
    </Provider>,
    document.getElementById('root')
  )
)
