export default {
  crowdsource: {
    displayText: 'Multiple select',
    defaultOptions: {
      explanatoryText: 'Tap the plus and minus buttons to record your vote against each item. You can change or withdraw your vote at any time while voting is active.',
      showScores: true,
      allowClientAdd: true
    },
    exportQuery: 'exportQuestion'
  },
  multipleChoice: {
    displayText: 'Multiple choice',
    defaultOptions: {
      explanatoryText: 'Tap the plus button next to your most preferred answer to record your vote. You can change your vote at any time while voting is active.',
      showScores: true,
      allowClientAdd: false
    },
    exportQuery: 'exportMultipleChoiceQuestion'
  },
  ratings: {
    displayText: 'Ratings',
    defaultOptions: {
      explanatoryText: 'Rate each item on a scale of 1 to 5. You can change your vote at any time while voting is active.',
      showScores: false,
      allowClientAdd: false
    },
    exportQuery: 'exportQuestion'
  }
}