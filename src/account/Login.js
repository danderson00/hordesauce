import React, { useState } from 'react'
import { auth } from '@x/unify.react'
import { Button, Form, Submit, Text } from '@danderson00/react-forms-material'
import { Dialog, DialogActions, DialogContent } from '@material-ui/core'

export default auth(function Login({ auth, onClose, open }) {
  const [creatingNew, setCreatingNew] = useState(false)

  const login = data => auth.authenticate({ ...data, provider: 'password' }, true)
    .then(({ success }) => success && onClose())

  const loginForm = (
    <Form onSubmit={login}>
      <DialogContent>
        <Text name="username" label="Username" required />
        <Text name="password" label="Password" type="password" required />
        <small>
          Haven't got an account?&nbsp;
          <button className="link" onClick={e => { setCreatingNew(true); e.preventDefault() }}>Create one</button>.
        </small>
      </DialogContent>
      <DialogActions>
        <Submit>Log In</Submit>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </Form>
  )

  const signup = data => auth.createUser({ ...data, provider: 'password' }, true).then(onClose)
  const signupForm = (
    <Form onSubmit={signup}>
      <DialogContent>
        <Text name="username" label="Username" required />
        <Text name="password" label="Password" type="password" required />
        <small>
          Already got an account?&nbsp;
          <button className="link" onClick={() => setCreatingNew(false)}>Log in</button>.
        </small>
      </DialogContent>
      <DialogActions>
        <Submit>Create Account</Submit>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </Form>
  )

  return (
    <Dialog open={open} onClose={onClose}>
      {creatingNew
        ? signupForm
        : loginForm
      }
    </Dialog>
  )
})