import React from 'react'
import { auth } from '@x/unify.react'
import { Button } from '@material-ui/core'
import { useAuth0 } from '@auth0/auth0-react'

export default auth(function Auth0Login({ auth }) {
  const { getAccessTokenWithPopup } = useAuth0()
  const login = () => getAccessTokenWithPopup()
    .then(auth0Token => auth.authenticate({ provider: 'auth0', auth0Token }, true))

  return (
    <Button onClick={login}>Login</Button>
  )
})
