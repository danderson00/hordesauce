import React, { useState } from 'react'
import { auth } from '@x/unify.react'
import { IconButton, ListItemIcon, ListItemText, Menu, MenuItem } from '@material-ui/core'
import AccountCircle from '@material-ui/icons/AccountCircle'
import ExitToApp from '@material-ui/icons/ExitToApp'
import { useAuth0 } from '@auth0/auth0-react'

export default auth(function AccountMenu({ auth }) {
  const [anchor, setAnchor] = useState(null)

  const { logout: auth0Logout } = useAuth0()

  const logout = () => {
    auth0Logout({ localOnly: true })
    auth.logout()
    setAnchor(null)
  }

  return <>
    <IconButton onClick={e => setAnchor(e.target)}>
      <AccountCircle fontSize="large" />
    </IconButton>
    <Menu
      anchorEl={anchor}
      open={Boolean(anchor)}
      onClose={() => setAnchor(null)}
    >
      <MenuItem onClick={logout}>
        <ListItemIcon><ExitToApp /></ListItemIcon>
        <ListItemText>Log out</ListItemText>
      </MenuItem>
    </Menu>
  </>
})