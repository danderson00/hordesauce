import React from 'react'

export default function Score({ value = 0, max = 10, min, width = 100 }) {
  const barWidth = min ? width / 2 : width

  const totalHeight = 11
  const barHeight = 5

  const lineColor = '#333333'
  const minColor = '#880000'
  const maxColor = '#005500'

  const styles = {
    container: {
      display: 'flex'
    },
    min: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      width: min ? barWidth : 0,
      height: totalHeight,
      borderLeft: min ? `1px solid ${lineColor}` : `none`,
    },
    minValue: {
      width: (min && value < 0) ? value / min * barWidth * -1 : 0,
      height: barHeight,
      background: minColor
    },
    max: {
      display: 'flex',
      alignItems: 'center',
      width: barWidth,
      height: totalHeight,
      borderLeft: `1px solid ${lineColor}`,
      borderRight: `1px solid ${lineColor}`
    },
    maxValue: {
      width: (max && value > 0) ? value / max * barWidth : 0,
      height: barHeight,
      background: maxColor
    }
  }

  return (
    <div style={styles.container}>
      <div style={styles.min}><div style={styles.minValue}></div></div>
      <div style={styles.max}><div style={styles.maxValue}></div></div>
    </div>
  )
}