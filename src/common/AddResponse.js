import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'
import { v4 as uuid } from 'uuid'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'

export default publisher(
  function AddResponse({ clientId, publish, open, onClose }) {
    const add = ({ text }) => publish('response', {
      clientId,
      clientName: localStorage.clientName,
      responseId: uuid(),
      respondedAt: Date.now(),
      text
    })
    .then(onClose)

    return (
      <Dialog open={open} onClose={onClose}>
        <Form onSubmit={add} resetOnSubmit margin="none">
          <DialogTitle>Add Response</DialogTitle>
          <DialogContent>
            <Text name="text" label="Response" required autoFocus />
          </DialogContent>
          <DialogActions>
            <Submit>Add Response</Submit>
            <Button onClick={onClose}>Cancel</Button>
          </DialogActions>
        </Form>
      </Dialog>
    )
  }
)