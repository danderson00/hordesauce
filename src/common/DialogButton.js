import React, { createElement, useState } from 'react'
import { Button, Fab } from '@material-ui/core'

export default function DialogButton({ children, component, variant, fullWidth, action, startIcon, ...props }) {
  const [open, setOpen] = useState(false)

  return <>
    {(action || action === '')
      ? <Fab onClick={() => setOpen(true)} className="action">
          {children}
        </Fab>
      : <Button
          variant={variant || 'contained'}
          fullWidth={fullWidth}
          onClick={() => setOpen(true)}
          startIcon={startIcon}
        >
          {children}
        </Button>
    }
    {createElement(component, {
      ...props,
      open,
      onClose: () => setOpen(false)
    })}
  </>
}
