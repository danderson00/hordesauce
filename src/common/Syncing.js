import React from 'react'

export default ({ size = 40, color = 'rgba(0, 0, 0, 0.3)', strokeWidth = 3.6 }) => (
  <div className="__xest_react-Syncing">
    <div style={{ width: size, height: size, color }}>
      <svg viewBox="22 22 44 44">
        <circle cx="44" cy="44" r="20.2" fill="none" strokeWidth={strokeWidth}></circle>
      </svg>
    </div>
  </div>
)

const style = document.createElement('style')
style.id = '__xest_react-Syncing'
style.innerText = `
.__xest_react-Syncing{
width:100%;
display:flex;
justify-content:center;
}
.__xest_react-Syncing div{
animation:__xest_react-rotate 1.4s linear infinite;
stroke:currentColor;
}
.__xest_react-Syncing circle{
animation:__xest_react-circular-dash 1.4s ease-in-out infinite;
stroke-dasharray:80px,200px;
stroke-dashoffset:0px;
}
@-webkit-keyframes __xest_react-rotate{
0%{transform-origin:50% 50%;}
100%{transform:rotate(360deg);}
}
@-webkit-keyframes __xest_react-circular-dash{
0%{stroke-dasharray:1px,200px;stroke-dashoffset:0px;}
50%{stroke-dasharray:100px,200px;stroke-dashoffset:-15px;}
100%{stroke-dasharray:100px,200px;stroke-dashoffset:-125px;}
}`
document.head.appendChild(style)