module.exports = ({ scoped, ownerOnly, scopeOwnerOnly }) => ({
  exportScore: o => o.accumulate().map(({ clientId, value }) => ({ clientId, value })),
  exportScores: o => o.topic('score').groupBy('clientId', o => o.exportScore()),

  exportResponse: o => o.compose(
    o => o.topic('response').accumulate().map(({ responseId, text }) => ({ responseId, response: text })),
    o => o.exportScores(),
    (response, scores) => ({ ...response, scores })
  ),
  exportResponses: o => o.groupBy('responseId', o => o.exportResponse()),

  exportQuestion: [
    scopeOwnerOnly('questionSetId'),
    scoped('questionSetId', 'questionId'),
    o => o.compose(
      o => o.topic('question').accumulate().map(({ questionId, text }) => ({ questionId, question: text })),
      o => o.exportResponses(),
      (question, responses) => ({ ...question, responses })
    )
  ],

  // need to work on polymorphism a bit - exportResponse should just call into the question types
  exportMultipleChoiceScores: o => o.topic('score').groupBy('clientId').map(({ clientId }) => ({ clientId, value: 1 })),
  exportMultipleChoiceResponse: o => o.compose(
    o => o.topic('response').accumulate().map(({ responseId, text }) => ({ responseId, response: text })),
    o => o.exportMultipleChoiceScores(),
    (response, scores) => ({ ...response, scores })
  ),
  exportMultipleChoiceResponses: o => o.compose(
    o => o.groupBy('responseId', o => o.topic('response').accumulate().map(({ responseId, text }) => ({ responseId, response: text }))),
    o => o.groupBy('clientId', o => o.topic('score')),
    (responses, scores) => responses.map(response => ({
      ...response,
      scores: scores
        .filter(x => x.responseId === response.responseId)
        .map(x => ({ clientId: x.clientId, value: 1 }))
    }))
  ),
  exportMultipleChoiceQuestion: [
    scopeOwnerOnly('questionSetId'),
    scoped('questionSetId', 'questionId'),
    o => o.compose(
      o => o.topic('question').accumulate().map(({ questionId, text }) => ({ questionId, question: text })),
      o => o.exportMultipleChoiceResponses(),
      (question, responses) => ({ ...question, responses })
    )
  ],

  exportQuestionComments: o => o.compose(
    o => o.topic('question').accumulate().map(({ questionId, text }) => ({ questionId, question: text })),
    o => o.topic('comment').groupBy('clientId').map(({ clientId, text }) => ({ clientId, comment: text })),
    (question, comments) => ({ ...question, comments })
  ),
  exportComments: [
    ownerOnly,
    scoped('questionSetId'),
    o => o.groupBy('questionId', o => o.exportQuestionComments())
  ]
})