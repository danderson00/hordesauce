const exportData = require('./exportData')

module.exports = ({ scoped, userScope, ownerOnly, scopeOwnerOnly }) => ({
  ...exportData({ scoped, ownerOnly, scopeOwnerOnly }),

  question: [scoped('questionSetId', 'questionId'),
    o => o.assign({
      '...details': o => o.topic('question').accumulate(),
      'responseCount': o => o.groupBy('responseId',
        undefined,
        o => o.topic('response').where(x => x.deleted)
      ).count()
    })
  ],
  questions: [scoped('questionSetId'), o => o.groupBy('questionId', o => o.question())],
  questionSet: [scoped('questionSetId'), o => o.assign({
    '...details': o => o.topic('questionSet').accumulate(),
    'code': o => o.topic('questionSetCode').select('questionSetCode'),
    'questionCount': o => o.groupBy('questionId').count()
  })],
  questionSets: [userScope, o => o.groupBy('questionSetId', o => o.questionSet())],

  selectedQuestionSetId: [userScope, o => o.topic('questionSetSelected').select('questionSetId')],
  selectedQuestionId: [scoped('questionSetId'), o => o.topic('questionSelected').select('questionId')],
  activeQuestionId: [scoped('questionSetId'), o => o.topic('questionActivated').select('questionId')],
  questionSetIdFromCode: [scoped('questionSetCode'), o => o.topic('questionSetCode').select('questionSetId')],

  crowdsourceResponse: o => o.assign({
    '...details': o => o.topic('response').accumulate(),
    'score': o => o.topic('score').groupBy('clientId').sum('value')
  }),
  crowdsource: [scoped('questionSetId', 'questionId'), o => o.compose(
    o => o.question(),
    o => o.groupBy('responseId',
        o => o.crowdsourceResponse(),
        o => o.topic('response').where(x => x.deleted)
      ),
    o => o.groupBy('responseId',
        o => o.topic('score').groupBy('clientId').sum('value').map(sum => ({ sum }))
      )
      .orderByDescending('sum')
      .mapAll(x => x.length > 0 ? x[0] && x[0].sum : 0),
    (question, responses, highestScore) => ({
      ...question,
      responses: responses.map(response => ({ ...response, max: highestScore, min: highestScore }))
    })
  )],
  crowdsourceScore: [scoped('questionSetId', 'questionId', 'responseId', 'clientId'),
    o => o.topic('score').select('value')
  ],

  multipleChoice: [scoped('questionSetId', 'questionId'), o => o.compose(
    o => o.question(),
    o => o.groupBy('responseId',
      o => o.topic('response').accumulate(),
      o => o.topic('response').where(x => x.deleted)
    ),
    o => o.topic('score').groupBy('clientId')
      .mapAll(x => x.reduce(
        (responses, { responseId }) => ({
          ...responses,
          [responseId]: (responses[responseId] || 0) + 1,
        }),
        {}
      )),
    o => o.topic('score').groupBy('clientId').count(),

    (question, responses, scores, totalResponses) => ({
      ...question,
      responses: responses.map(response => ({
        ...response,
        score: (scores && scores[response.responseId]) || 0,
        max: totalResponses
      }))
    })
  )],
  multipleChoiceSelectedResponse: [
    scoped('questionSetId', 'questionId', 'clientId'),
    o => o.topic('score').select('responseId')
  ],

  ratingsResponse: o => o.compose(
    o => o.topic('response').accumulate(),
    o => o.topic('score').groupBy('clientId').average('value'),
    (details, score) => ({ ...details, score, max: 5 })
  ),
  ratings: [scoped('questionSetId', 'questionId'), o => o.assign({
    '...question': o => o.question(),
    'responses': o => o.groupBy('responseId',
      o => o.ratingsResponse(),
      o => o.topic('response').where(x => x.deleted)
    )
  })],
  ratingsScore: [scoped('questionId'), o => o.topic('score').select('value')]
})
