module.exports = {
  allowRawQueries: false,
  strictTypes: true,
  strictApi: true,
  requireSsl: false,
  scopes: ['questionSetId', 'questionSetCode', 'questionId', 'responseId', 'clientId', 'userId'],
  storage: { client: 'sqlite3', connection: { filename: 'test.db' } },
  auth: { secret: 'xxx', password: true },
  ownedScopes: ['questionSetId']
}
