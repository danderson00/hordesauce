// const { logWriters: { console, file } } = require('@x/unify')

module.exports = {
  allowRawQueries: true,
  strictTypes: true,
  strictApi: false,
  scopes: ['questionSetId', 'questionSetCode', 'questionId', 'responseId', 'clientId', 'userId'],
  storage: { client: 'sqlite3', connection: { filename: 'hordesauce.db' } },
  auth: { secret: 'hordesauce', password: true },
  ownedScopes: ['questionSetId'],

  // log: {
  //   level: 'debug',
  //   writers: [
  //     console(),
  //     file({ destination: 'logs/test.log', rotate: true })
  //   ]
  // }
}
