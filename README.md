# hordesauce

Open source idea and feedback crowdsourcing built on Unify.

## Installation

```shell
git clone https://gitlab.com/danderson00/hordesauce.git
cd hordesauce
yarn # or `npm install`
```

## Usage

```shell
yarn start # or npm start
```

This will open your default browser to the landing page of the application.

In order to create question sets, you must create an account. Click the `Log in` button at the top right of the 
screen and click the `Create account` button. Enter your username and password and click `Create account`. Once your 
account has been created, you are able to create question sets and individual questions within that set.

Question sets are assigned 4 alphanumeric character code that can be used by participants to access the question set 
by entering the code into the text input field on the landing page and clicking "Go". 

Questions can be one of multiple choice (select one response from a predefined set of answers), multiple select 
(select multiple responses from a predefined set of answers) or a rating of 1 - 5 for each item on the question.

Each question type can be configured to allow participants to add their own responses, allowing other participants 
to up or down vote each response. The overall participant responses rate can also be made visible or hidden from 
view at any time. 

