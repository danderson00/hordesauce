const { logWriters: { console, file } } = require('@x/unify')
const { readFileSync } = require('fs')

module.exports = {
  allowRawQueries: false,
  strictTypes: true,
  strictApi: true,
  scopes: ['questionSetId', 'questionSetCode', 'questionId', 'responseId', 'clientId', 'userId'],
  log: {
    writers: [
      console(),
      file({ destination: 'logs/hordesauce.log', rotate: true })
    ]
  },
  storage: {
    client: 'pg',
    connection: {
      host: 'hordesauce-pg-sgp-do-user-0000000-0.a.db.ondigitalocean.com',
      port: 25060,
      user: 'doadmin',
      password: 'xxx',
      database: 'hordesauce',
      ssl: {
        ca: readFileSync(__dirname + '/hordesauce-pg.crt')
      }
    }
  },
  auth: {
    secret: 'xxx',
    auth0: { domain: 'yourdomain.auth0.com' }
  },
  letsEncrypt: {
    domain: 'yourdomain.com',
    email: 'email@yourdomain.com',
    configDir: '~/.acme'
  },
  ownedScopes: ['questionSetId'],
  socket: { throttle: { timeout: 100 } }
}
