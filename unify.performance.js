const xlog = require('@x/log')
const publishingWriter = require('@x/unify.test/src/common/publishingWriter')
const middleware = require('@x/unify.test/src/common/middleware')

const operationLogger = xlog({
  level: 'debug',
  writers: [
    publishingWriter({ url: 'ws://localhost:1235' }),
    xlog.writers.console({ level: 'info' })
  ],
  scope: { timestamp: () => Date.now(), source: 'hordesauce.middleware' }
})

module.exports = {
  development: true,
  startDevelopmentServer: true,
  allowRawQueries: false,
  strictTypes: true,
  strictApi: true,
  scopes: ['questionSetId', 'questionSetCode', 'questionId', 'responseId', 'clientId', 'userId'],
  ownedScopes: ['questionSetId'],
  log: {
    writers: [
      xlog.writers.console(),
      publishingWriter({ url: 'ws://localhost:1235' })
    ]
  },
  storage: { client: 'sqlite', connection: { filename: 'performance.db' } },
  auth: { secret: 'hordesauce', password: true },
  extendHost: host => host.use(middleware(operationLogger))
}
